const express = require('express')
const cors = require('cors')
const routes = require('./routes')

const app = express()
const port = 3333

app.use(cors())
// convert body request to json
app.use(express.json())
app.use(routes)
app.listen(port, () => console.log(`Example app listening on port ${port}!`))