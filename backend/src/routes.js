const express = require('express')
const OngsCtrl = require('./controllers/ong.controller')
const IncidentsCtrl = require('./controllers/incident.controller')
const ProfileCtrl = require('./controllers/profile.controller')
const SessionCtrl = require('./controllers/session.controller')

const conn = require('./database/connection')

const routes = express.Router()

routes.post('/sessions', SessionCtrl.create)

routes.get('/ongs', OngsCtrl.index)
routes.post('/ongs', OngsCtrl.create)

routes.get('/profile', ProfileCtrl.index)

routes.get('/incidents', IncidentsCtrl.index)
routes.post('/incidents', IncidentsCtrl.create)
routes.delete('/incidents/:id', IncidentsCtrl.delete)

module.exports = routes;