import React from 'react';

export default function Hedaer({children, title}) {
  return (
    <header>
      <h1>{title}</h1>
      <small>{children}</small>
    </header>
  )
}